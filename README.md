# API for Code Generation and Refactoring #

This repository contains Bachelor's thesis text for Czech Technical University in Prague - Faculty of Information Technology - Department of Software Engineering. Printed and submitted version is marked with tag #thesis_submission.

Complete source codes with brief documentation are located at https://bitbucket.org/jnesveda/refactoring_custom .

### Abstract ###

During coding a programmer can face many repetitive tasks such as renaming
variables, creating methods to implement an interface or creating test cases.
This can be tedious and time consuming so we designed and implemented API
for code generation and refactoring in Smalltalk/X. Although STX contains
tools for such a task, they do not provide much user-friendly API. In this work we present how is API designed and how a programmer can use this API to
create code generators or refactorings.